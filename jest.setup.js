// Learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom'
import '@testing-library/react'

global.matchMedia =
  global.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      addEventListener: function () {},
      removeEventListener: function () {},
      removeListener: function () {},
    }
  }
