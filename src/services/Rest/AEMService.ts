import HTTPService from '@/services/Rest/HttpService'

export default class AemService extends HTTPService {
  async getJson(slug: string) {
    try {
      const { data, status }: any = await this.$axios.get(
        `personal/prepago/${slug}?revalidate`,
        {
          validateStatus: () => true,
        }
      )
      if (status >= 400) {
        const ErrorInfo = JSON.stringify({
          message: 'Fallo la peticion - endpoint de AEM',
          method: 'get',
          path: `/${slug}`,
          status: status || 'NO STATUS',
          data: data,
        })
        throw new Error(ErrorInfo)
      }
      return { json: data, slug }
    } catch (e) {
      const ErrorInfo = JSON.stringify({
        message: 'Fallo la peticion - endpoint de AEM',
        method: 'get',
        path: `/${slug}`,
        status: status,
        error: e,
      })
      throw new Error(ErrorInfo)
    }
  }

  async getJson404(slug: string) {
    const { data, status }: any = await this.$axios.get(`errors/${slug}`, {
      validateStatus: () => true,
    })
    if (status >= 400) {
      throw new Error(data)
    }
    return { json: data, slug }
  }
}
