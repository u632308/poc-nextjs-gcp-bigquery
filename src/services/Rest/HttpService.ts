import Axios, { AxiosInstance } from 'axios'

export default class HttpService {
  headers: {
    Accept: string
    'Content-Type': string
    Authorization?: string
  }

  $axios: AxiosInstance

  constructor(config: any) {
    this.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      // 'Access-Control-Allow-Origin': '*'
    }
    this.$axios = Axios.create({
      ...config,
      headers: {
        ...config.headers,
        ...this.headers,
      },
    })
  }
}
