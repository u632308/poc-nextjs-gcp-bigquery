export const mockGet = jest.fn()

const mock = jest.fn().mockImplementation(() => {
  return {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    $axios: {
      get: mockGet,
    },
  }
})

export default mock
