import AEMService from './Rest/AEMService'

export const dependencies = {
  AEMService: new AEMService({
    baseURL: process.env.AEM_BASE_URL_PROXI,
  }),
}
