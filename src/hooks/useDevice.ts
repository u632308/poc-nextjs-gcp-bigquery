import { useState } from 'react'

const checkMobile = () => {
  if (typeof window !== 'undefined' && window?.navigator) {
    const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /iPad/i,
      /iPod/i,
      /BlackBerry/i,
      /Windows Phone/i,
    ]
    const mobileCheck = toMatch.some((toMatchItem) => {
      return navigator.userAgent.match(toMatchItem)
    })
    return mobileCheck
  }
  return false
}

const useDevice = () => {
  const [isMobile] = useState(() => checkMobile())
  return { isMobile }
}
export default useDevice
