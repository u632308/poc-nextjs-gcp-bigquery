export interface AemContainerJsonTypes {
  description?: string
  canonicalUrl?: string
  templateName?: string
  cssClassNames?: string
  seoProperties?: any[]
  favicon?: string
  styleSheets?: any[]
  language?: string
  title?: string
  columnClassNames?: string
  ':itemsOrder'?: any[]
  ':items'?: any
  ':type'?: string
  ':hierarchyType'?: string
  ':path'?: string
}

export interface AemComponentTypes {
  json: any
  columnClassName: string
  getComponent: AemComponentObject
}

interface AemComponentObject {
  [key: string]: (json: any) => JSX.Element
}

export interface AemContainerTypes {
  json: AemContainerJsonTypes
  getComponent: AemComponentObject
}
