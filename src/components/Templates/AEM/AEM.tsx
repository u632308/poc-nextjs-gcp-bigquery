/* eslint-disable react/display-name */
// @ts-nocheck

import Head from 'next/head'
import dynamic from 'next/dynamic'

const PreHeader = dynamic(() => import('remote/PreHeader'))
const Header = dynamic(() => import('remote/Header'))
const Prefooter = dynamic(() => import('remote/Prefooter'))
const FooterLegal = dynamic(() => import('remote/FooterLegal'))
const Footer = dynamic(() => import('remote/Footer'))
const LogoBanner = dynamic(() => import('remote/LogoBanner'))
const Banner = dynamic(() => import('@/components/Molecules/Banner'))
const HtmlContent = dynamic(() => import('@/components/Molecules/HtmlContent'))
const TitleContent = dynamic(
  () => import('@/components/Molecules/TitleContent')
)
const Jumbotron = dynamic(() => import('@/components/Molecules/Jumbotron'))

const AEM = ({ json }: { json: any }) => {
  return (
    <>
      <Head>
        <title>{json?.title || 'Error'}</title>
        <meta
          name="description"
          content={json?.description || 'Error'}
          key="desription"
        />
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width, maximum-scale=5.0"
          key="viewport"
        />
        <meta name="robots" content="noindex, nofollow" />
        <link
          rel="canonical"
          href={json?.canonicalUrl || '/'}
          key="canonical"
        />
        <link rel="icon" href={json?.favicon || 'favicon.ico'} key="icon" />
      </Head>
      {json && (
        <>
          <div
            className={json.cssClassNames}
            data-landing-name={
              json.dataLandingName === 'donde-comprar-chip'
                ? 'donde-comprar-chip'
                : ''
            }
          >
            <header className="aem_wrapper__header">
              <RenderDynamicComponent components={json.components.header} />
            </header>
            <main className="aem_wrapper__main">
              <div className="inside">
                <RenderDynamicComponent components={json.components.inside} />
              </div>
            </main>
            <footer className="aem_wrapper__footer">
              <RenderDynamicComponent components={json.components.footer} />
            </footer>
          </div>
        </>
      )}
    </>
  )
}

const componentsMap = {
  PreHeader,
  Header,
  Footer,
  FooterLegal,
  Prefooter,
  LogoBanner,
  Banner,
  TitleContent,
  Jumbotron,
  HtmlContent,
}

const RenderDynamicComponent = ({ components }) => {
  return components.map((component, index) => {
    const { name, props } = component
    const DynamicComponent = componentsMap[name]
    return (
      <div key={`aemmap_${name}_${index}`} className="aem_wrapper_element">
        <DynamicComponent {...props} />
      </div>
    )
  })
}

export default AEM
