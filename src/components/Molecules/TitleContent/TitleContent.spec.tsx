/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
import React from 'react'
import { render } from '../../../../tests/test-utils'
import TitleContent from './TitleContent'
import { TitleContentTypes } from './types'

const props: TitleContentTypes = {
  json: {
    visible: true,
    title:
      'Conexión Total: beneficios y descuentos con tus servicios Personal Flow',
    description: 'Descubrí lo que necesitás para disfrutarlos',
    ':type': 'teco-cms-ecosystem/components/title-description',
  },
}

describe('Molecules/TitleContent', () => {
  test('TitleContent should render correctly', () => {
    const { container } = render(<TitleContent {...props} />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
