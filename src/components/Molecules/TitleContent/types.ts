export interface TitleContentListTypes {
  visible: boolean
  overline?: string
  title: string
  description: string
  ':type': string
}

export interface TitleContentTypes {
  json: TitleContentListTypes
}
