import { TitleContentTypes } from './types'

const TitleContent = ({ json }: TitleContentTypes) => {
  return (
    <div className="title__content">
      {json.overline && (
        <h1 className="title__content-overline">{json.overline}</h1>
      )}
      {json.title && <h1 className="title__content-title">{json.title}</h1>}
      {json.description && (
        <p className="title__content-subtitle">{json.description}</p>
      )}
    </div>
  )
}

export default TitleContent
