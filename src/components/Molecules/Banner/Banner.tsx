import HtmlRenderer from '@/components/Atoms/HtmlRenderer'
import { BannerTypes } from './types'
import useDevice from '@/hooks/useDevice'

const Wrapper = ({ children, link }: { children; json: BannerTypes }) => {
  if (link.url) {
    return (
      <a href={link.url} target={link.target} style={{ cursor: 'pointer' }}>
        {children}
      </a>
    )
  }
  return <>{children}</>
}

const Banner = ({ json }: BannerTypes) => {
  const { isMobile } = useDevice()

  return (
    <Wrapper link={json.link}>
      <div className="banner_container">
        <div
          className={`${isMobile ? 'banner-mobile' : 'banner-desktop'}`}
          style={{
            width: '100%',
            height: '100%',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundImage: `url(${
              isMobile && json.mobileImage
                ? process.env.AEM_BASE_PATH_IMAGES + json.mobileImage.path
                : process.env.AEM_BASE_PATH_IMAGES + json.webImage.path
            })`,
          }}
        >
          <div className="banner-content">
            {json.leadTitle && (
              <HtmlRenderer className="leadTitle" html={json.leadTitle} />
            )}
            {json.downloadTitle && (
              <HtmlRenderer
                className="downloadTitle"
                html={json.downloadTitle}
              />
            )}
            {json.downloadText && (
              <HtmlRenderer className="downloadText" html={json.downloadText} />
            )}
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export default Banner
