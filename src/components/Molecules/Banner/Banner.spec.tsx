/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import Banner from './Banner'
import { BannerTypes } from './types'

const mockResponse = jest.fn()
Object.defineProperty(window, 'navigator', {
  value: {
    userAgent: 'Android',
    assign: mockResponse,
  },
  writable: true,
})

const json: BannerTypes = {
  visible: true,
  height: '200',
  backgroundType: 'gradient',
  webImage: {
    path: '/content/dam/teco-cms-ecosystem/personal/contaco/banner-hero-conexion-total-desk.png',
    altText: 'banner hero conexion total desk',
    img: true,
  },
  mobileImage: {
    path: '/content/dam/teco-cms-ecosystem/personal/contaco/banner-hero-conexion-total-mob.png',
    altText: 'banner hero conexion total mob',
    img: true,
  },
  leadTitle: '<p>PERSONAL EN TU CASA, EN TU CELU Y FLOW</p>\r\n',
  downloadTitle: '<p>Conexión Total</p>\r\n',
  downloadText:
    '<p>La manera de conectarnos cambia. Las conexiones verdaderas se mantienen</p>\r\n',
  link: {
    title: 'sarasa',
    url: 'https://www.personal.com.ar/',
    target: '_blank',
    isImg: false,
  },
  ':type': 'teco-cms-ecosystem/components/relevantinfobanner',
  priority: true,
}

describe('Molecules/Banner', () => {
  test('Banner should render correctly in desktop mode', () => {
    const { container } = render(<Banner json={json} />)
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Banner should render correctly and show the pointer in case of having link', () => {
    render(<Banner json={json} />)
    expect(screen.getByRole('link')).toHaveAttribute(
      'style',
      'cursor: pointer;'
    )
  })

  test('Banner should render correctly and not show the pointer in case of not having link', () => {
    json.link.url = undefined
    const { container } = render(<Banner json={json} />)
    const el = container.querySelector('a')
    expect(el).toBeNull()
  })

  test('Banner should render correctly in mobile mode', () => {
    const { container } = render(<Banner json={json} />)
    const el = container.querySelector('.banner-mobile')
    expect(el).toBeInTheDocument()
  })

  test('Banner should render correctly in desktop mode and show the correct image', () => {
    Object.defineProperty(window, 'navigator', {
      value: {
        userAgent: 'desktop',
        assign: mockResponse,
      },
      writable: true,
    })

    const { container } = render(<Banner json={json} />)
    const el = container.querySelector('.banner-desktop')
    // screen.debug()
    expect(el).toHaveAttribute(
      'style',
      'width: 100%; height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover; background-image: url(https://publish-p32363-e109126.adobeaemcloud.com/content/dam/teco-cms-ecosystem/personal/contaco/banner-hero-conexion-total-desk.png);'
    )
  })
})
