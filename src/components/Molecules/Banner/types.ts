interface imgTypes {
  path: string
  altText: string
}

export interface BannerTypes {
  id: string
  visible: boolean
  json: {
    id: string
    webImage: imgTypes
    mobileImage: imgTypes
    leadTitle: string
    downloadTitle: string
    downloadText: string
    link: {
      url: string | undefined
      target: string
    }
  }
}
