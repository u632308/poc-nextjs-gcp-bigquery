import { useEffect } from 'react'
import { JumbotronTypes } from './types'
import HtmlRenderer from '@/components/Atoms/HtmlRenderer'
import Img from '@/components/Atoms/Img/Img'
import { getRemoveTagsHtml } from '@/utils/getRemoveTagsHtml'
import useDevice from '@/hooks/useDevice'


const chooseTypeOfVariant = (variant, json) =>
  'jumbotron container' + (variant === '404' ? ' jumbotron404' : '') + (json.alignment === 'background' ? ' background' : '')

const Jumbotron = ({ json, variant = '' }: JumbotronTypes) => {
  const isMobile = useDevice()

  useEffect(() => {
    if (variant === '404') {
      const gtmTrack = async () => {
        const { GTMTrackError, gtmErrorType } = await import('@/utils/gtm')
        GTMTrackError(gtmErrorType.SYSTEM_ERROR, json.plainTitle, 'Page Not Found', 404)
      }
      gtmTrack()
    }
  }, [variant, json])

  if (!json) return <></>

  let attBackground = {
    'data-ee-type': 'promo',
    'data-ee-creative': '',
    'data-ee-id': isMobile ? json.mobileImage?.path : json.desktopImage?.path,
    'data-ee-name': '',
    'data-ee-position': 1,
  }

  if (json.alignment === 'background') {
    attBackground = {
      ...attBackground,
      'data-ee-name': `banner ${json.plainTitle ? getRemoveTagsHtml(json.plainTitle) : ''} `,
      'data-ee-creative': 'banner',
    }
  } else {
    attBackground = {
      ...attBackground,
      'data-ee-creative': 'Jumbotron',
      'data-ee-name': `Jumbotron ${json.plainTitle ? getRemoveTagsHtml(json.plainTitle) : ''} `,
    }
  }

  const attButton = { 'data-ee-element': 'promoClick' }

  return (
    <div
      {...attBackground}
      className={json.alignment === 'right'
        ? 'jumbotron img-right'
        : chooseTypeOfVariant(variant, json)}
    >

      {json.visibleCta &&
        <div className={`valign-wrapper ${json.alignment === 'background' ? 'background-wrapper' : ''}`}>
          <div className={`valign-wrapper__content`}>
            {json.leadTitle && <HtmlRenderer html={json.leadTitle} className='valign-wrapper__leadtitle' />}
            {json.title && <HtmlRenderer html={json.title} className='valign-wrapper__title' />}
            {json.description && <HtmlRenderer html={json.description} className='valign-wrapper__text' />}
            <a className={`label${variant}`}
              href={json.buttonUrl}
              target={json.linkType}
              style={{
                backgroundColor: json.ctaColor,
                borderColor: json.ctaColor,
                color: json.ctaTextColor || undefined
              }}
              {...attButton}
            >
              <p>{json.buttonLabel}</p>
            </a>
          </div>
        </div>
      }
      <div className={`jumbotron__img ${json.alignment === 'background' ? 'background-img hide-on-large-only' : 'show-on-small hide-on-med-and-up'}`}>
        <Img
          src={json.mobileImage?.path}
          alt={json.mobileImage?.altText}
        />
      </div>
      <div
        className={`jumbotron__img ${json.alignment === 'background' ? 'background-img hide-on-med-and-down' : 'show-on-large hide-on-small-only'}`}>
        <Img
          src={json.desktopImage?.path}
          alt={json.desktopImage?.altText}
        />
      </div>
    </div >
  )
}

export default Jumbotron