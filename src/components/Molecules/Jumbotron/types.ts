export interface JumbotronTypes {
  json: JumbotronDataTypes | undefined
  variant: string
  slug?: string
}

export interface JumbotronDataTypes {
  alignment: string
  title?: string
  plainTitle?: string
  description?: string
  desktopImage?: DesktopImageTypes
  mobileImage?: MobileImageTypes
  visibleJumboTron?: boolean
  buttonLabel?: string
  linkType?: string
  buttonUrl?: string
  imagesLinks?: any
  leadTitle?: string
  ctaColor?: string
  visibleCta?: boolean
  notToCallOfferId?: boolean
  ctaTextColor?: string
  priority?: boolean
  ':type'?: string
}

export interface DesktopImageTypes {
  path: string
  altText: string
  img?: boolean
}

export interface MobileImageTypes {
  path: string
  altText: string
  img?: boolean
}
