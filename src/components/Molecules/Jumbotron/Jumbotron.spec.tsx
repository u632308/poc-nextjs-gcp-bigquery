import { render } from '@testing-library/react'
import Jumbotron from './Jumbotron'
import { JumbotronTypes } from './types'

const props: JumbotronTypes = {
  "variant": undefined,
  "json": {
    "visibleJumboTron": true,
    "title": "<p>No encontramos la</p>\r\n<p>página que estás buscando</p>\r\n",
    "plainTitle": "No encontramos lapágina que estás buscando",
    "desktopImage": {
      "path": "/content/dam/teco-cms-ecosystem/errors/404-desk.png",
      "altText": "404 desk",
      "img": true
    },
    "mobileImage": {
      "path": "/content/dam/teco-cms-ecosystem/errors/404-mobile.png",
      "altText": "404 mobile",
      "img": true
    },
    "alignment": "right",
    "visibleCta": true,
    "buttonLabel": "Volver al inicio",
    "buttonUrl": "https://www.personal.com.ar/",
    "linkType": "_blank",
    "notToCallOfferId": true,
    "imagesLinks": [
      {
        "image": {
          "path": "fal fa-box-heart",
          "altText": null,
          "img": false
        },
        "link": "https://www.personal.com.ar/ofertas",
        "target": "_self",
        "text": "Ofertas y combos",
        "img": false
      },
      {
        "image": {
          "path": "fal fa-wifi",
          "altText": null,
          "img": false
        },
        "link": "https://www.personal.com.ar/internet",
        "target": "_self",
        "text": "Servicios de internet",
        "img": false
      },
      {
        "image": {
          "path": "/content/dam/teco-cms-ecosystem/errors/portabilidad.svg",
          "altText": "portabilidad",
          "img": true
        },
        "link": "https://conectividad.personal.com.ar/portabilidad",
        "target": "_self",
        "text": "Portabilidad",
        "img": true
      },
      {
        "image": {
          "path": "fal fa-tv-alt",
          "altText": null,
          "img": false
        },
        "link": "https://entretenimiento.flow.com.ar/tv-cable",
        "target": "_self",
        "text": "TV y Streaming",
        "img": false
      }
    ],
    ":type": "teco-cms-ecosystem/components/jumbotron"
  },
  "slug": "404.model.json"
}

describe('Pages/404', () => {
  test('Jumbotron should render correctly', () => {
    const { container } = render(
      <Jumbotron {...props} />
    )
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Jumbotron with variant 404 and background alignment should render correctly', () => {
    const testProps = {
      ...props,
      ...{
        variant: '404',
        json: {
          ...props.json,
          alignment: 'background'
        }
      }
    }
    const { container } = render(
      <Jumbotron {...testProps} />
    )
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Jumbotron with left alignment should render correctly', () => {
    const testProps = {
      ...props,
      ...{
        json: {
          ...props.json,
          alignment: 'left'
        }
      }
    }
    const { container } = render(
      <Jumbotron {...testProps} />
    )
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Jumbotron render correctly with json is empty', () => {
    const testProps = {
      ...props,
      ...{
        json: undefined,
        variant: ''
      }
    }
    const { container } = render(
      <Jumbotron {...testProps} />
    )
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Jumbotron with variant background should render data-ee-name correctly', () => {
    const testProps = {
      ...props,
      ...{
        variant: '404',
        json: {
          ...props.json,
          plainTitle: undefined,
          alignment: 'background'
        }
      }
    }
    const { container } = render(
      <Jumbotron {...testProps} />
    )
    expect(container.firstChild).toMatchSnapshot()
    const el = container.querySelector('.jumbotron')
    expect(el).toHaveAttribute("data-ee-name", "banner  ")
  })

  test('Jumbotron with variant left should render data-ee-name correctly', () => {
    const testProps = {
      ...props,
      ...{
        json: {
          ...props.json,
          plainTitle: undefined,
          alignment: 'left'
        }
      }
    }
    const { container } = render(
      <Jumbotron {...testProps} />
    )
    expect(container.firstChild).toMatchSnapshot()
    const el = container.querySelector('.jumbotron')
    expect(el).toHaveAttribute("data-ee-name", "Jumbotron  ")
  })
})
