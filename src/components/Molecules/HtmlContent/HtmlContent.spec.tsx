/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
import React from 'react'
import { render } from '../../../../tests/test-utils'
import HtmlContent from './HtmlContent'
import { props } from './mock'

describe('Molecules/HtmlContent', () => {
  test('HtmlContent should render correctly', () => {
    const { container } = render(<HtmlContent {...props} />)
    expect(container.firstChild).toMatchSnapshot()
  })
})
