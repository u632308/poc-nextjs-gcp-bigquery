/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
import React from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import HtmlContent from './HtmlContent'
import { props } from './mock'
import { strToHtml } from '@/utils'

describe('Molecules/HtmlContent', () => {
  test('HtmlContent should have a "html-content" class', () => {
    render(<HtmlContent {...props} />)
    const element = screen.getByRole('html')
    expect(element).toHaveClass(props.className)
  })
  test('HtmlContent should show content', () => {
    render(<HtmlContent {...props} />)
    const element = screen.getByRole('html')
    expect(element.firstElementChild).toEqual(
      strToHtml(props.json.html).firstElementChild
    )
  })
})
