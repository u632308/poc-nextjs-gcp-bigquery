import { HtmlContentTypes } from './types'
import HtmlRenderer from '../../Atoms/HtmlRenderer'

const HtmlContent = ({ json }: HtmlContentTypes) => {
  return <HtmlRenderer html={json.html} className={'html-content'} />
}

export default HtmlContent
