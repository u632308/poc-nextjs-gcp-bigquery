import { render, screen } from '@testing-library/react'
import Img from './Img'

describe('Img Component', () => {
  test('renders Img component', () => {
    render(<Img src="test.jpg" alt="Test Image" />)
    const imgElement = screen.getByAltText('Test Image')
    expect(imgElement).toBeInTheDocument()
  })
  test('renders Img component with rounded class', () => {
    render(<Img src="test.jpg" alt="Test Image" rounded />)
    const imgElement = document.getElementsByClassName('image--rounded')
    expect(imgElement).not.toBeNull()
  })
  test('renders Img component with custom class', () => {
    render(<Img src="test.jpg" alt="Test Image" className='customImgClass' />)
    const imgElement = document.getElementsByClassName('customImgClass')
    expect(imgElement).not.toBeNull()
  })
  test('renders Img component with absolute path', () => {
    render(<Img src="test.jpg" alt="Test Image" absolutePath />)
    const imgElement = screen.getByAltText('Test Image')
    const srcElement = imgElement.getAttribute('src')
    expect(srcElement).toBe('test.jpg')
  })
})
