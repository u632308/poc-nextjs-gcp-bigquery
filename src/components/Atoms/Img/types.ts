import { ImageProps as ImagePropsNext } from 'next/image'

export interface ImageTypes {
  src?: string
  alt?: string
  width?: number
  height?: number
  rounded?: boolean
  className?: string
  absolutePath?: boolean
}
