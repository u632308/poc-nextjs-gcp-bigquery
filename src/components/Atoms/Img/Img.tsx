/* eslint-disable @next/next/no-img-element */
import { ImageTypes } from './types'

const Img = ({
  src,
  alt,
  rounded = false,
  className,
  absolutePath = false,
  ...rest
}: ImageTypes) => {
  let srcPath = process.env.CDN_IMAGES_DOMAIN
  if (process.env.NODE_ENV === 'test') {
    srcPath = 'https://publish-p32363-e119350.adobeaemcloud.com'
  }
  return (
    <div
      className={
        'image-container' +
        (rounded ? ' image--rounded' : '') +
        (className ? ' ' + className : '')
      }
    >
      <img src={absolutePath ? src : `${srcPath}${src}`} alt={alt} {...rest} />
    </div>
  )
}

export default Img
