import HtmlRenderer from './HtmlRenderer'
import { render } from '@testing-library/react'

describe('Atoms/HtmlRenderer', () => {
  test('HtmlRenderer should render correctly', () => {
    const { container } = render(
      <HtmlRenderer
        html={'<p>HTML</p>'}
      />
    )
    expect(container.firstChild).toMatchSnapshot()
  })
})
