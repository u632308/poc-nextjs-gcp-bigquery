import React from 'react'

export interface HtmlRendererTypes {
  html: React.ReactNode | any
  className?: string
}
