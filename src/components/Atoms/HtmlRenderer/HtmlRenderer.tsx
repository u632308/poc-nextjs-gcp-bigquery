import DOMPurify from 'isomorphic-dompurify'
import { HtmlRendererTypes } from './types'

const HtmlRenderer = ({ html, className }: HtmlRendererTypes) => (
  <div
    role="html"
    dangerouslySetInnerHTML={{
      __html: DOMPurify.sanitize(html, { ADD_ATTR: ['target'] }),
    }}
    className={className}
  />
)

export default HtmlRenderer
