export interface CarouselTypes {
  settings: any
  className: string
  children?: React.ReactNode
}
