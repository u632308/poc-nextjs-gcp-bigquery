import React from 'react'
import Slider from 'react-slick'
import { CarouselTypes } from './types'

const Carousel = ({ settings, className, children = <></> }: CarouselTypes) => {
  return (
    <div className={className}>
      <Slider {...settings}>{children}</Slider>
    </div>
  )
}

export default Carousel
