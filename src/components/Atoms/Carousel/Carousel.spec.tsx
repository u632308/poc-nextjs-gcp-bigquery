/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
import React from 'react'
import { render } from '@testing-library/react'
import Carousel from './Carousel'

describe('Carousel', () => {
  test('Carousel should render correctly', () => {
    const { container } = render(
      <Carousel settings={'test'} className={'test'} />
    )
    expect(container.firstChild).toMatchSnapshot()
    expect(container).not.toBe(undefined)
  })
})
