import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document'

import {
  FlushedChunks,
  flushChunks,
  revalidate,
} from '@module-federation/nextjs-mf/utils'

const globalThis: any = global
Object.assign(globalThis, {
  webpackChunkLoad: async (url: string | URL | Request) => {
    const res = await fetch(url, {
      headers: {
        'bypass-secret': `${process.env.AWS_SECRET_BP}`,
      },
    })
    return res
  },
})

Object.assign(globalThis, {
  fetch: new Proxy(globalThis.fetch, {
    apply: function (target, that, args) {
      args[1] = {
        ...args[1],
        headers: { 'bypass-secret': `${process.env.AWS_SECRET_BP}` },
      }
      const temp = target.apply(that, args)
      return temp
    },
  }),
})

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    await revalidate()
    const initialProps = await Document.getInitialProps(ctx)
    const chunks = await flushChunks()

    return {
      ...initialProps,
      chunks,
    }
  }
  render() {
    return (
      <Html lang="es-AR">
        <Head>
          {/* @ts-ignore */}
          <FlushedChunks chunks={this.props.chunks} />
        </Head>
        <body suppressHydrationWarning={true}>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
