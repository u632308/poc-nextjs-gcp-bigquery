import AEM_LANDINGS from '@/utils/constants/links'
import AEM from '@/components/Templates/AEM'
import Script from 'next/script'
import { AEMJsonFilter } from '@/utils/constants/AEMJsonFilter'
import { env } from 'process'
import { dependencies } from '@/services'

interface landingTypes {
  json: any
  gtmCode: string
  gtmEnv: string
  isGtmEnvActive: boolean
}

function Home({ json, gtmCode, gtmEnv, isGtmEnvActive }: landingTypes) {
  return (
    <>
      <AEM json={json} />
      {/* Google Tag Manager */}
      <Script id="gtm">
        {`try{(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});
        var f=d.getElementsByTagName(s)[0],j=d.createElement(s),
        dl=l!='dataLayer'?'&l='+l:'';j.async=true;
        j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl${
          isGtmEnvActive ? '+' + "'" + gtmEnv + "'" : ''
        };
        f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','${gtmCode}');}catch(e){}`}
      </Script>
      {/* End Google Tag Manager */}
      {/* Google Tag Manager (noscript) */}
      <noscript
        dangerouslySetInnerHTML={{
          __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=${gtmCode}${
            isGtmEnvActive ? gtmEnv : ''
          }" 
        height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
        }}
      />
      {/* End Google Tag Manager (noscript) */}
    </>
  )
}

export async function getStaticProps() {
  const slug = AEM_LANDINGS.landing['dondeComprarChip'].slug
  try {
    const json = await dependencies.AEMService.getJson(
      `${slug}.model.json`
    ).then(({ json }) => AEMJsonFilter(json, slug))
    return {
      props: {
        json,
        gtmCode: process.env.GTM_CODE,
        gtmEnv: process.env.GTM_ENV,
        isGtmEnvActive: process.env.ACTIVE_GTM_ENV === 'true',
      },
      revalidate: 120,
    }
  } catch (error) {
    console.error(error)
    return {
      props: {
        json: {},
        gtmCode: '',
        gtmEnv: '',
        isGtmEnvActive: false,
      },
    }
  }
}

export default Home
