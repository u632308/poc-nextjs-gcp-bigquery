import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const secret = req.query.secret
  if (secret !== process.env.NEXT_TOKEN_REVALIDATE) {
    console.info('Invalid revalidate token')
    return res.status(401).json({ message: 'Invalid token' })
  }
  try {
    // this should be the actual path not a rewritten path
    // e.g. for "/blog/[slug]" this should be "/blog/post-1"
    await res.revalidate('/donde-comprar-chip')
    console.info({ revalidated: true, now: Date.now() })
    return res.json({ revalidated: true, now: Date.now() })
  } catch (err) {
    // If there was an error, Next.js will continue
    // to show the last successfully generated page
    console.error('Error revalidating')
    return res.status(500).send('Error revalidating')
  }
}
