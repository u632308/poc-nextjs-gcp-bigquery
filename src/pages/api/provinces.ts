import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const provinces = ['Capital Federal', 'Córdoba', 'Santa Fe', 'Tucumán']
    return res.json({
      provinces,
    })
  } catch (err) {
    console.error('Error getting provinces')
    return res.status(500).send('Error getting provinces')
  }
}
