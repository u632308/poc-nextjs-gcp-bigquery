import { BigQuery } from '@google-cloud/bigquery';
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const projectId = process.env.GOOGLE_CLOUD_PROJECT_ID_TECO;
  const clientEmail = process.env.GOOGLE_CLOUD_CLIENT_EMAIL_TECO;
  const privateKey = process.env.GOOGLE_CLOUD_PRIVATE_KEY_TECO;

  if (!projectId || !clientEmail || !privateKey) {
    res.status(500).json({ error: 'Missing Google Cloud environment variables' });
    return;
  }

   const bigquery = new BigQuery({
    projectId: projectId,
    credentials: {
      client_email: clientEmail,
      private_key: privateKey.replace(/\\n/g, '\n')
    },
  });

  const query = 'SELECT * FROM `teco-prod-ds-comercial-8a96.teco_prepago.DG_Semaforo` LIMIT 10';
  const options = {
    query: query
  };

  try {
    const [rows] = await bigquery.query(options);

    res.status(200).json(rows);
  } catch (error) {
    console.error('Error al ejecutar la consulta:', error);
    res.status(500).json({ error: error });
  }
}
