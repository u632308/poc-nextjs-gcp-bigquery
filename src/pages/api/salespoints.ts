import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const { province, locality } = req.query

    const salespoints = [
      {
        province: 'Capital Federal',
        name: 'Capital Federal',
        salespoint: 'Salepoint 1 Captal Federal',
        address: 'Peru 123',
        has_stock: true,
        is_official: true,
        x: 100,
        y: 200,
      },
      {
        province: 'Capital Federal',
        name: 'Capital Federal',
        salespoint: 'Salepoint 2 Captal Federal',
        address: 'Charcas 3344',
        has_stock: false,
        is_official: false,
        x: 200,
        y: 400,
      },
      {
        province: 'Capital Federal',
        name: 'Capital Federal',
        salespoint: 'Salepoint 3 Captal Federal',
        address: 'Malabia 423',
        has_stock: true,
        is_official: true,
        x: 300,
        y: 600,
      },
      {
        province: 'Córdoba',
        name: 'Rio Cuarto',
        salespoint: 'Salepoint 1 Rio Cuarto',
        address: 'San Martín 3432',
        has_stock: true,
        is_official: true,
        x: 400,
        y: 800,
      },
      {
        province: 'Córdoba',
        name: 'Rio Cuarto',
        salespoint: 'Salepoint 2 Rio Cuarto',
        address: 'Cardales 873',
        has_stock: false,
        is_official: false,
        x: 500,
        y: 1000,
      },
      {
        province: 'Córdoba',
        name: 'Carlos Paz',
        salespoint: 'Salepoint 1 Carlos Paz',
        address: 'Belgrano 2311',
        has_stock: true,
        is_official: true,
        x: 600,
        y: 1200,
      },
      {
        province: 'Santa Fe',
        name: 'Rosario',
        salespoint: 'Salepoint 1 Rosario',
        address: 'Emilio Mitre 2233',
        has_stock: true,
        is_official: true,
        x: 700,
        y: 1400,
      },
      {
        province: 'Santa Fe',
        name: 'Rosario',
        salespoint: 'Salepoint 2 Rosario',
        address: 'Colombia 2233',
        has_stock: false,
        is_official: false,
        x: 800,
        y: 1600,
      },
      {
        province: 'Santa Fe',
        name: 'Rosario',
        salespoint: 'Salepoint 3 Rosario',
        address: 'Bouchard 33454',
        has_stock: true,
        is_official: true,
        x: 900,
        y: 1800,
      },
      {
        province: 'Santa Fe',
        name: 'Sauce Viejo',
        salespoint: 'Salepoint 1 Sauce Viejo',
        address: 'Mitre 1033',
        has_stock: true,
        is_official: true,
        x: 1000,
        y: 2000,
      },
      {
        province: 'Santa Fe',
        name: 'Sauce Viejo',
        salespoint: 'Salepoint 2 Sauce Viejo',
        address: 'Garzón 2233',
        has_stock: false,
        is_official: false,
        x: 1100,
        y: 2200,
      },
      {
        province: 'Tucuman',
        name: 'Famaillá',
        salespoint: 'Salepoint 1 Famaillá',
        address: 'Humahuaca 3344',
        has_stock: true,
        is_official: true,
        x: 1200,
        y: 2400,
      },
      {
        province: 'Tucuman',
        name: 'Famaillá',
        salespoint: 'Salepoint 2 Famaillá',
        address: 'Membrillar 44',
        has_stock: false,
        is_official: false,
        x: 1300,
        y: 2600,
      },
      {
        province: 'Tucuman',
        name: 'Famaillá',
        salespoint: 'Salepoint 3 Famaillá',
        address: 'Av Rivadavia 4455',
        has_stock: true,
        is_official: true,
        x: 1400,
        y: 2800,
      },
      {
        province: 'Tucuman',
        name: 'Graneros',
        salespoint: 'Salepoint 1 Graneros',
        address: 'Ayacucho 4433',
        has_stock: true,
        is_official: true,
        x: 100,
        y: 200,
      },
      {
        province: 'Tucuman',
        name: 'Graneros',
        salespoint: 'Salepoint 2 Graneros',
        address: 'Venezuela 1614',
        has_stock: false,
        is_official: false,
        x: 200,
        y: 400,
      },
      {
        province: 'Tucuman',
        name: 'Graneros',
        salespoint: 'Salepoint 3 Graneros',
        address: 'Ecuador 1416',
        has_stock: true,
        is_official: true,
        x: 300,
        y: 600,
      },
    ]

    let filteredSalespoints = salespoints

    if (province) {
      filteredSalespoints = filteredSalespoints.filter(
        (salespoint) =>
          salespoint.province.toLowerCase() ===
          (province as string).toLowerCase()
      )
    }

    if (locality) {
      filteredSalespoints = filteredSalespoints.filter(
        (salespoint) =>
          salespoint.name.toLowerCase() === (locality as string).toLowerCase()
      )
    }

    const response = {
      locality: locality || '',
      province: province || '',
      salespoints: filteredSalespoints.map((salespoint) => ({
        address: salespoint.address,
        has_stock: salespoint.has_stock,
        is_official: salespoint.is_official,
        name: salespoint.salespoint,
        x: salespoint.x,
        y: salespoint.y,
      })),
    }

    return res.json(response)
  } catch (err) {
    console.error('Error getting salespoints', err)
    return res.status(500).send('Error getting salespoints')
  }
}
