import { NextApiRequest, NextApiResponse } from 'next'

type LocalitiesResponse = {
  province: string
  localities: string[]
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const { province = 'Capital Federal' } = req.query

    const localities = [
      {
        province: 'Capital Federal',
        localities: ['Capital Federal'],
      },
      {
        province: 'Córdoba',
        localities: ['Rio Cuarto', 'Carlos Paz'],
      },
      {
        province: 'Entre Rios',
        localities: ['Colón', 'Concordia'],
      },
      {
        province: 'Santa Fe',
        localities: ['Rosario', 'Sauce Viejo'],
      },
      {
        province: 'Tucumán',
        localities: ['Famaillá', 'Graneros'],
      },
    ]

    const filteredLocalities = localities.find(
      (locality) =>
        locality.province.toLowerCase() === (province as string).toLowerCase()
    )

    const responseLocalities: LocalitiesResponse = {
      province: province as string,
      localities: filteredLocalities ? filteredLocalities.localities : [],
    }

    return res.json(responseLocalities)
  } catch (err) {
    console.error('Error getting localities', err)
    return res.status(500).send('Error getting localities')
  }
}
