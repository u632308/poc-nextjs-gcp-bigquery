export const getRemoveTagsHtml = (text: string | null) => {
  if (text === null || text === '' || text === undefined) return false

  const matches = text.match(/<\w+>/g)

  // Si el texto tiene más de una etiqueta html
  if (matches?.length >= 2) {
    const regexp = /<h[2-4]>([^<]*)<\/h[2-4]>/

    const matchText = text.match(regexp)
    if (!matchText) return false

    return matchText[0].replace(/<\/?h[2-4]>/g, '')
  }

  // Si el texto tiene solo una etiqueta html
  return text.replace(/(<([^>]+)>)/gi, '').trim()
}
