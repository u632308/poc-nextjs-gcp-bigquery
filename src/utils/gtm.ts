declare global {
  interface Window {
    dataLayer: any
  }
}

const executeDataLayer = (eventDate: { event: string; landingName?: string; errorType?: any; errorMessage?: any; errorContext?: any; errorCode?: any; userData?: { attributes: { is_fan: boolean; is_convergent: boolean; is_cobre: boolean }; customer: { gender: string | undefined; document_type: string | undefined; document: string | undefined; suscriber_id: string | undefined } } | { attributes: { is_fan: boolean; is_convergent: boolean; is_cobre: boolean }; customer: { gender: string | undefined; document_type: string | undefined; document: string | undefined; suscriber_id: string | undefined } } | { attributes: { is_fan: boolean; is_convergent: boolean; is_cobre: boolean }; customer: { gender: string | undefined; document_type: string | undefined; document: string | undefined; suscriber_id: string | undefined } }; optimizeInfo?: any; event_name?: string; event_params?: { segment_product: any } }) => {
  window?.dataLayer?.push(eventDate)
}

export const gtmErrorType = {
  USER_ERROR: 'err1',
  SYSTEM_ERROR: 'err2',
  BUSINESS_ERROR: 'err3',
}

export const GTMTrackError = (
  errorType: string | null,
  errorMessage: string | null,
  errorContext: string | null,
  errorCode: string | null
) => {
  const event = {
    event: 'error',
    landingName: 'home',
    errorType: errorType || undefined,
    errorMessage: errorMessage || undefined,
    errorContext: errorContext || undefined,
    errorCode: errorCode || undefined,
  }

  executeDataLayer(event)
  return event
}

export const GTMTrackSignIn = (
  gender: string,
  document_type: string,
  document: string,
  suscriber_id: string
) => {
  const eventLoguin = {
    event: 'signIn',
    userData: {
      attributes: {
        is_fan: false,
        is_convergent: false,
        is_cobre: !document,
      },
      customer: {
        gender: gender || undefined,
        document_type: document_type || undefined,
        document: document || undefined,
        suscriber_id: suscriber_id || undefined,
      },
    },
  }
  executeDataLayer(eventLoguin)
  return eventLoguin
}

export const GTMTrackAuthentication = (
  gender: string,
  document_type: string,
  document: string,
  suscriber_id: string
) => {
  const eventAuthentication = {
    event: 'authentication',
    userData: {
      attributes: {
        is_fan: false,
        is_convergent: false,
        is_cobre: !document,
      },
      customer: {
        gender: gender || undefined,
        document_type: document_type || undefined,
        document: document || undefined,
        suscriber_id: suscriber_id || undefined,
      },
    },
  }
  executeDataLayer(eventAuthentication)
  return eventAuthentication
}

export const GTMTrackSignOut = (
  gender: string,
  document_type: string,
  document: string,
  suscriber_id: string
) => {
  const eventSignOut = {
    event: 'signOut',
    userData: {
      attributes: {
        is_fan: false,
        is_convergent: false,
        is_cobre: !document,
      },
      customer: {
        gender: gender || undefined,
        document_type: document_type || undefined,
        document: document || undefined,
        suscriber_id: suscriber_id || undefined,
      },
    },
  }
  executeDataLayer(eventSignOut)
  return eventSignOut
}

export const GTMTrackOptimize = (value: any) => {
  const eventCdpOptimize = {
    event: 'activateOptimize',
    optimizeInfo: value,
  }
  executeDataLayer(eventCdpOptimize)
  return eventCdpOptimize
}
export const GTMTrackOptimizeGA4 = (value: any) => {
  const eventCdpOptimizeG4 = {
    event: 'ga4.trackEvent',
    event_name: 'experiment_optimize',
    event_params: {
      segment_product: value,
    },
  }
  executeDataLayer(eventCdpOptimizeG4)
  return eventCdpOptimizeG4
}
