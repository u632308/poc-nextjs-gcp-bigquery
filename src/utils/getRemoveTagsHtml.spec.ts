import { getRemoveTagsHtml } from './getRemoveTagsHtml'

describe('getRemoveTagsHtml', () => {
  test('debería eliminar todas las etiquetas HTML de un string con un Tag', () => {
    const text = '<h2>Pasate a Personal</h2>'
    //const text = '<h7></h7><h7></h7><h7><h7>'
    expect(getRemoveTagsHtml(text)).toBe('Pasate a Personal')
  })

  test('debería eliminar todas las etiquetas HTML de un string con varios Tags', () => {
    const text = '<h2>Pasate a Personal</h2><p>COntrata Flow</p>'
    expect(getRemoveTagsHtml(text)).toBe('Pasate a Personal')
  })

  test('debería devolver falso si la entrada es nula o está vacía', () => {
    expect(getRemoveTagsHtml(null)).toBe(false)
    expect(getRemoveTagsHtml('')).toBe(false)
  })

  test('debería devolver false cuando los tags son incorrectos', () => {
    const text = '<h7></h7><h7></h7><h7><h7>'
    expect(getRemoveTagsHtml(text)).toBe(false)
  })
})
