export const getTextFromHtml = (htmlString: string) => {
  const regexDeleteHtmlTags = /<.+?>/g
  return htmlString && htmlString.replace(regexDeleteHtmlTags, '')
}

export const strToHtml = (value: string, tag = 'div') => {
  const str = value
  const htmlObject = document.createElement(tag)
  htmlObject.innerHTML = str
  return htmlObject
}
