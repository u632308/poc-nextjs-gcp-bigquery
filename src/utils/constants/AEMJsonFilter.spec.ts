import { AEMJsonFilter } from './AEMJsonFilter'

const json = {
  description: 'Description donde-comprar-chip @TODO',
  favicon: '/content/dam/teco-cms-ecosystem/assets/favicon-personal.png',
  styleSheets: [],
  dataLandingName: 'donde-comprar-chip',
  showHotJar: 'false',
  addMaze: 'false',
  seoProperties: [],
  jsonConfigs: [],
  scripts: [],
  templateName: 'home-page-template',
  canonicalUrl: 'https://www.personal.com.ar/donde-comprar-chip',
  cssClassNames: 'page-spa page basicpage',
  title: 'Atención al Cliente | Personal Flow',
  ':items': {
    root: {
      columnCount: 12,
      columnClassNames: {
        responsivegrid: 'aem-GridColumn aem-GridColumn--default--12',
      },
      gridClassNames: 'aem-Grid aem-Grid--12 aem-Grid--default--12',
      ':itemsOrder': [
        'experiencefragment',
        'responsivegrid',
        'experiencefragment_1633736698',
      ],
      ':items': {
        experiencefragment: {
          ':itemsOrder': ['root'],
          ':items': {
            root: {
              columnCount: 12,
              columnClassNames: {
                headerlevelone: 'aem-GridColumn aem-GridColumn--default--12',
                headerlevelzero: 'aem-GridColumn aem-GridColumn--default--12',
              },
              gridClassNames: 'aem-Grid aem-Grid--12 aem-Grid--default--12',
              ':itemsOrder': ['headerlevelzero', 'headerlevelone'],
              ':items': {
                headerlevelzero: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/headerLevelZero',
                },
                headerlevelone: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/headerLevelOne',
                },
              },
            },
          },
        },
        responsivegrid: {
          columnCount: 12,
          columnClassNames: {
            relevantinfobanner: 'aem-GridColumn aem-GridColumn--default--12',
          },
          gridClassNames: 'aem-Grid aem-Grid--12 aem-Grid--default--12',
          ':items': {
            relevantinfobanner: {
              visible: true,
              ':type': 'teco-cms-ecosystem/components/relevantinfobanner',
            },
            jumbotron: {
              visibleJumboTron: true,
              ':type': 'teco-cms-ecosystem/components/jumbotron',
            },
            jumbotron_248797633: {
              visibleJumboTron: true,
              ':type': 'teco-cms-ecosystem/components/jumbotron',
            },
            title_description: {
              visible: true,
              ':type': 'teco-cms-ecosystem/components/title-description',
            },
          },
          ':itemsOrder': [
            'relevantinfobanner',
            'jumbotron',
            'jumbotron_248797633',
            'title_description',
          ],
          ':type': 'wcm/foundation/components/responsivegrid',
        },
        experiencefragment_1633736698: {
          ':itemsOrder': ['root'],
          ':items': {
            root: {
              columnCount: 12,
              columnClassNames: {
                footerlevelone: 'aem-GridColumn aem-GridColumn--default--12',
                footerleveltwo: 'aem-GridColumn aem-GridColumn--default--12',
                legalfooter: 'aem-GridColumn aem-GridColumn--default--12',
                logobanner: 'aem-GridColumn aem-GridColumn--default--12',
              },
              gridClassNames: 'aem-Grid aem-Grid--12 aem-Grid--default--12',
              ':itemsOrder': [
                'logobanner',
                'footerlevelone',
                'footerleveltwo',
                'legalfooter',
              ],
              ':items': {
                logobanner: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/logobanner',
                },
                footerlevelone: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerlevelone',
                },
                footerleveltwo: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerleveltwo',
                },
                legalfooter: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/legalfooter',
                },
              },
            },
          },
        },
      },
      ':type': 'wcm/foundation/components/responsivegrid',
    },
  },
  ':type': 'teco-cms-ecosystem/components/page-spa',
  ':itemsOrder': ['root'],
  ':hierarchyType': 'page',
  ':path': '/content/teco-cms-ecosystem/ar/es/personal/donde-comprar-chip-next',
}
describe('AEMJsonFilter', () => {
  test('Should return an array with components objects ordered', () => {
    ;(json.description = 'test'),
      (json.favicon = 'test'),
      (json.pageId = 'test'),
      (json.showHotJar = 'test'),
      (json.canonicalUrl = 'test'),
      (json.title = 'test'),
      (json.dataLandingName = 'test'),
      expect(AEMJsonFilter(json, '', true)).toEqual({
        description: 'test',
        favicon: 'test',
        pageId: 'test',
        showHotJar: 'test',
        canonicalUrl: 'test',
        title: 'test',
        dataLandingName: 'test',
        components: {
          header: [
            {
              name: 'PreHeader',
              props: {
                iconNameCart: '',
                idpClientID: `${process.env.IDP_CLIENT_ID}`,
                idpRedirectUri: `${process.env.IDP_REDIRECT_URI}`,
                idpSilentRedirectUri: `${process.env.IDP_SILENT_REDIRECT_URI}`,
                json: {
                  ':type': 'teco-cms-ecosystem/components/headerLevelZero',
                  visible: true,
                },
                urlCart: '',
                urlTargetCart: '',
                variant: undefined,
                visibleCart: false,
              },
            },
            {
              name: 'Header',
              props: {
                json: {
                  ':type': 'teco-cms-ecosystem/components/headerLevelOne',
                  visible: true,
                },
              },
            },
          ],
          inside: [
            {
              name: 'Banner',
              props: {
                json: {
                  visible: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/relevantinfobanner',
                },
              },
            },
            {
              name: 'Jumbotron',
              props: {
                json: {
                  visibleJumboTron: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/jumbotron',
                },
                variant: '',
              },
            },
            {
              name: 'Jumbotron',
              props: {
                json: {
                  visibleJumboTron: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/jumbotron',
                },
                variant: '',
              },
            },
            {
              name: 'TitleContent',
              props: {
                json: {
                  visible: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/title-description',
                },
                richText: true,
              },
            },
          ],
          footer: [
            {
              name: 'LogoBanner',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/logobanner',
                },
              },
            },
            {
              name: 'Prefooter',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerlevelone',
                },
              },
            },
            {
              name: 'Footer',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerleveltwo',
                },
              },
            },
            {
              name: 'FooterLegal',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/legalfooter',
                },
              },
            },
          ],
        },
      })
  })

  test('Should return an array with components objects ordered and head data null', () => {
    ;(json.description = undefined),
      (json.favicon = undefined),
      (json.pageId = undefined),
      (json.showHotJar = undefined),
      (json.canonicalUrl = undefined),
      (json.title = undefined),
      (json.dataLandingName = undefined),
      expect(AEMJsonFilter(json, '', true)).toEqual({
        description: null,
        favicon: null,
        pageId: null,
        showHotJar: null,
        canonicalUrl: null,
        title: null,
        dataLandingName: null,
        components: {
          header: [
            {
              name: 'PreHeader',
              props: {
                iconNameCart: '',
                idpClientID: `${process.env.IDP_CLIENT_ID}`,
                idpRedirectUri: `${process.env.IDP_REDIRECT_URI}`,
                idpSilentRedirectUri: `${process.env.IDP_SILENT_REDIRECT_URI}`,
                json: {
                  ':type': 'teco-cms-ecosystem/components/headerLevelZero',
                  visible: true,
                },
                urlCart: '',
                urlTargetCart: '',
                variant: undefined,
                visibleCart: false,
              },
            },
            {
              name: 'Header',
              props: {
                json: {
                  ':type': 'teco-cms-ecosystem/components/headerLevelOne',
                  visible: true,
                },
              },
            },
          ],
          inside: [
            {
              name: 'Banner',
              props: {
                json: {
                  visible: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/relevantinfobanner',
                },
              },
            },
            {
              name: 'Jumbotron',
              props: {
                json: {
                  visibleJumboTron: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/jumbotron',
                },
                variant: '',
              },
            },
            {
              name: 'Jumbotron',
              props: {
                json: {
                  visibleJumboTron: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/jumbotron',
                },
                variant: '',
              },
            },
            {
              name: 'TitleContent',
              props: {
                json: {
                  visible: true,
                  priority: true,
                  ':type': 'teco-cms-ecosystem/components/title-description',
                },
                richText: true,
              },
            },
          ],
          footer: [
            {
              name: 'LogoBanner',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/logobanner',
                },
              },
            },
            {
              name: 'Prefooter',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerlevelone',
                },
              },
            },
            {
              name: 'Footer',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/footerleveltwo',
                },
              },
            },
            {
              name: 'FooterLegal',
              props: {
                json: {
                  visible: true,
                  ':type': 'teco-cms-ecosystem/components/legalfooter',
                },
              },
            },
          ],
        },
      })
  })

  test('Should return an array with components objects ordered without components visible false', () => {
    ;(json.description = undefined),
      (json.favicon = undefined),
      (json.pageId = undefined),
      (json.showHotJar = undefined),
      (json.canonicalUrl = undefined),
      (json.title = undefined),
      (json.dataLandingName = undefined),
      (json[':items']['root'][':items']['responsivegrid'][':items'][
        'relevantinfobanner'
      ].visible = false)
    expect(AEMJsonFilter(json, '', true)).toEqual({
      description: null,
      favicon: null,
      pageId: null,
      showHotJar: null,
      canonicalUrl: null,
      title: null,
      dataLandingName: null,
      components: {
        header: [
          {
            name: 'PreHeader',
            props: {
              iconNameCart: '',
              idpClientID: `${process.env.IDP_CLIENT_ID}`,
              idpRedirectUri: `${process.env.IDP_REDIRECT_URI}`,
              idpSilentRedirectUri: `${process.env.IDP_SILENT_REDIRECT_URI}`,
              json: {
                ':type': 'teco-cms-ecosystem/components/headerLevelZero',
                visible: true,
              },
              urlCart: '',
              urlTargetCart: '',
              variant: undefined,
              visibleCart: false,
            },
          },
          {
            name: 'Header',
            props: {
              json: {
                ':type': 'teco-cms-ecosystem/components/headerLevelOne',
                visible: true,
              },
            },
          },
        ],
        inside: [
          {
            name: 'Jumbotron',
            props: {
              json: {
                visibleJumboTron: true,
                priority: true,
                ':type': 'teco-cms-ecosystem/components/jumbotron',
              },
              variant: '',
            },
          },
          {
            name: 'Jumbotron',
            props: {
              json: {
                visibleJumboTron: true,
                priority: true,
                ':type': 'teco-cms-ecosystem/components/jumbotron',
              },
              variant: '',
            },
          },
          {
            name: 'TitleContent',
            props: {
              json: {
                visible: true,
                priority: true,
                ':type': 'teco-cms-ecosystem/components/title-description',
              },
              richText: true,
            },
          },
        ],
        footer: [
          {
            name: 'LogoBanner',
            props: {
              json: {
                visible: true,
                ':type': 'teco-cms-ecosystem/components/logobanner',
              },
            },
          },
          {
            name: 'Prefooter',
            props: {
              json: {
                visible: true,
                ':type': 'teco-cms-ecosystem/components/footerlevelone',
              },
            },
          },
          {
            name: 'Footer',
            props: {
              json: {
                visible: true,
                ':type': 'teco-cms-ecosystem/components/footerleveltwo',
              },
            },
          },
          {
            name: 'FooterLegal',
            props: {
              json: {
                visible: true,
                ':type': 'teco-cms-ecosystem/components/legalfooter',
              },
            },
          },
        ],
      },
    })
  })
})
