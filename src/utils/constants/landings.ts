const LANDINGS = {
  dondeComprarChip: {
    whoiam: false,
    service: [],
    slug: 'donde-comprar-chip',
    path: 'dode-comprar-chip',
  },
  message404: { whoiam: true, service: 'false', slug: '404' },
}

export default LANDINGS
