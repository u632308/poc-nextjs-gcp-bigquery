import { BannerTypes } from '@/components/Molecules/Banner/types'

const componentsType = [
  {
    name: 'header',
    types: {
      'teco-cms-ecosystem/components/headerLevelZero': (json: any) => {
        return {
          name: 'PreHeader',
          props: {
            json,
            idpClientID: `${process.env.IDP_CLIENT_ID}`,
            idpRedirectUri: `${process.env.IDP_REDIRECT_URI}`,
            idpSilentRedirectUri: `${process.env.IDP_SILENT_REDIRECT_URI}`,
            visibleCart: json.vari || false,
            iconNameCart: json.cartIcon || '',
            urlCart: json.cartUrl || '',
            urlTargetCart: json.cartTarget || '',
            variant: json.variant,
          },
        }
      },
      'teco-cms-ecosystem/components/headerLevelOne': (json: any) => {
        return {
          name: 'Header',
          props: {
            json,
          },
        }
      },
    },
  },
  {
    name: 'inside',
    types: {
      'teco-cms-ecosystem/components/title-description': (json: any) => {
        return {
          name: 'TitleContent',
          props: {
            json,
            richText: true,
          },
        }
      },
      'teco-cms-ecosystem/components/relevantinfobanner': (
        json: BannerTypes
      ) => {
        return {
          name: 'Banner',
          props: {
            json,
          },
        }
      },
      'teco-cms-ecosystem/components/jumbotron': (json: any, slug: any) => {
        return {
          name: 'Jumbotron',
          props: {
            json,
            variant: slug,
          },
        }
      },
      'teco-cms-ecosystem/components/html-content': (json: any) => {
        return {
          name: 'HtmlContent',
          props: {
            json,
            richText: true,
          },
        }
      },
    },
  },
  {
    name: 'footer',
    types: {
      'teco-cms-ecosystem/components/logobanner': (json: any) => {
        return {
          name: 'LogoBanner',
          props: {
            json,
          },
        }
      },
      'teco-cms-ecosystem/components/footerlevelone': (json: any) => {
        return {
          name: 'Prefooter',
          props: {
            json,
          },
        }
      },
      'teco-cms-ecosystem/components/footerleveltwo': (json: any) => {
        return {
          name: 'Footer',
          props: {
            json,
          },
        }
      },
      'teco-cms-ecosystem/components/legalfooter': (json: any) => {
        return {
          name: 'FooterLegal',
          props: {
            json,
          },
        }
      },
    },
  },
]

const filteredJson = (json, slug, arr, componentsType) => {
  json &&
    json[':itemsOrder']?.map((item: any) => {
      const component = filteredJsonItem(
        json[':items'][item],
        slug,
        arr,
        componentsType
      )
      if (component) {
        if (
          component['visible'] !== undefined ||
          component['visibleJumboTron'] !== undefined ||
          component['isVisible'] !== undefined
        ) {
          if (
            component['visible'] ||
            component['visibleJumboTron'] ||
            component['isVisible']
          ) {
            if (componentsType.types[component[':type']]) {
              const { name, props } = componentsType.types[component[':type']](
                component,
                slug
              )
              arr[componentsType.name].push({ name, props })
            }
          }
        } else {
          if (componentsType.types[component[':type']]) {
            const { name, props } = componentsType.types[component[':type']](
              component,
              slug
            )
            arr[componentsType.name].push({ name, props })
          }
        }
      }
    })
  return arr
}

const filteredJsonItem = (json, slug, arr, componentsType) => {
  if (
    json[':itemsOrder'] &&
    json[':type'] !== 'teco-cms-ecosystem/components/tabs'
  ) {
    filteredJson(json, slug, arr, componentsType)
    return null
  }
  return json
}

export const AEMJsonFilter = (json: any = {}, slug = '', isHeadData = true) => {
  const components = {
    header: [],
    inside: [],
    footer: [],
  }

  let headData = {}
  if (isHeadData) {
    const {
      description,
      favicon,
      pageId,
      showHotJar,
      canonicalUrl,
      title,
      dataLandingName,
    } = json

    headData = {
      description: description || null,
      favicon: favicon || null,
      pageId: pageId || null,
      showHotJar: showHotJar || null,
      canonicalUrl: canonicalUrl || null,
      title: title || null,
      dataLandingName: dataLandingName || null,
    }
  }
  componentsType.forEach((el) => filteredJson(json, slug, components, el))
  components.inside.slice(0, 5).forEach((el: { props?: { json: any } }) => {
    if (el.props && el.props.json) {
      el.props.json['priority'] = true
    }
  })
  const data = { ...headData, components }
  return data
}
