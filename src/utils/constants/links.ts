import LANDINGS from './landings'

const LINKS = {
  home: LANDINGS.dondeComprarChip.slug,
  typeComponentUrlAem: 'teco-cms-ecosystem/components/',
  landing: LANDINGS,
}

export default LINKS
