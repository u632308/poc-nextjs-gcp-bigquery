import { GTMTrackAuthentication, GTMTrackError, GTMTrackOptimize, GTMTrackOptimizeGA4, GTMTrackSignIn, GTMTrackSignOut } from "./gtm"

describe('GTM', () => {

  test('GTM track error should return correctly', () => {
    const errorType = "error";
    const errorMessage = "error";
    const errorContext = "error";
    const errorCode = "500";

    expect(GTMTrackError(errorType, errorMessage, errorContext, errorCode)).toEqual(expect.objectContaining({
      event: expect.any(String),
      landingName: expect.any(String),
      errorType: expect.any(String),
      errorMessage: expect.any(String),
      errorContext: expect.any(String),
      errorCode: expect.any(String),
    }));
  })

  test('GTM track error with undefined values should return incorrectly', () => {
    expect(GTMTrackError(null, null, null, null)).toEqual(expect.objectContaining({
      event: expect.any(String),
      landingName: expect.any(String),
      errorType: undefined,
      errorMessage: undefined,
      errorContext: undefined,
      errorCode: undefined,
    }));
  })

  test('GTMTrackSignIn, GTMTrackAuthentication and GTMTrackSignOut should return correctly', () => {
    const gender = "gender";
    const document_type = "document_type";
    const document = "document";
    const suscriber_id = "123456";

    const objectContainingToValue = expect.objectContaining({
      event: expect.any(String),
      userData: expect.objectContaining({
        attributes: expect.objectContaining({
          is_fan: expect.any(Boolean),
          is_convergent: expect.any(Boolean),
          is_cobre: expect.any(Boolean),
        }),
        customer: expect.objectContaining({
          gender: expect.any(String),
          document_type: expect.any(String),
          document: expect.any(String),
          suscriber_id: expect.any(String),
        })
      })
    });

    expect(GTMTrackSignIn(gender, document_type, document, suscriber_id)).toEqual(objectContainingToValue);
    expect(GTMTrackAuthentication(gender, document_type, document, suscriber_id)).toEqual(objectContainingToValue);
    expect(GTMTrackSignOut(gender, document_type, document, suscriber_id)).toEqual(objectContainingToValue);
  })

  test('GTMTrackSignIn, GTMTrackAuthentication and GTMTrackSignOut with undefined values should return correctly', () => {
    const objectContainingToValue = expect.objectContaining({
      event: expect.any(String),
      userData: expect.objectContaining({
        attributes: expect.objectContaining({
          is_fan: expect.any(Boolean),
          is_convergent: expect.any(Boolean),
          is_cobre: expect.any(Boolean),
        }),
        customer: expect.objectContaining({
          gender: undefined,
          document_type: undefined,
          document: undefined,
          suscriber_id: undefined,
        })
      })
    });

    expect(GTMTrackSignIn(null, null, null, null)).toEqual(objectContainingToValue);
    expect(GTMTrackAuthentication(null, null, null, null)).toEqual(objectContainingToValue);
    expect(GTMTrackSignOut(null, null, null, null)).toEqual(objectContainingToValue);
  })

  test('GTMTrackOptimize should return correctly', () => {
    const value = "value";

    expect(GTMTrackOptimize(value)).toEqual(expect.objectContaining({
      event: expect.any(String),
      optimizeInfo: value,
    }));
  })

  test('GTMTrackOptimizeGA4 should return correctly', () => {
    const value = "value";

    expect(GTMTrackOptimizeGA4(value)).toEqual(expect.objectContaining({
      event: expect.any(String),
      event_name: expect.any(String),
      event_params: {
        segment_product: value
      }
    }));
  })
})