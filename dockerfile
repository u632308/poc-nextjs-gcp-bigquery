FROM node:18.17.0
EXPOSE 30000
WORKDIR /landing
COPY . /landing

RUN yarn
CMD [ "yarn", "start" ]
