import {
  RenderOptions,
  RenderResult,
  render as rtlRender,
} from '@testing-library/react'
import { ReactElement } from 'react'

interface ProvidersProps {
  children: ReactElement
}

const Providers = ({ children }: ProvidersProps) => {
  return <>{children}</>
}

export const render = (
  ui: ReactElement,
  options?: RenderOptions
): RenderResult => rtlRender(ui, { wrapper: Providers, ...options })
