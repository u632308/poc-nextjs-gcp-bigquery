/* @type {import('next').NextConfig} */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const NextFederationPlugin = require('@module-federation/nextjs-mf')

const nextConfig = {
  env: {
    AEM_BASE_URL: process.env.AEM_BASE_URL,
    AEM_BASE_URL_PROXI: process.env.AEM_BASE_URL_PROXI,
    AEM_BASE_PATH_IMAGES: process.env.AEM_BASE_PATH_IMAGES,
    CDN_IMAGES_DOMAIN: process.env.CDN_IMAGES_DOMAIN,
    LANDING_PATH: process.env.LANDING_PATH,
    IDP_BASE_URL: process.env.IDP_BASE_URL,
    IDP_CLIENT_ID: process.env.IDP_CLIENT_ID,
    GTM_ENV: process.env.GTM_ENV,
    GTM_CODE: process.env.GTM_CODE,
    ACTIVE_GTM_ENV: process.env.ACTIVE_GTM_ENV,
    APP_ENV: process.env.APP_ENV,
    MICROFRONTEND_BASE_URL: process.env.MICROFRONTEND_BASE_URL,
    IDP_REDIRECT_URI: process.env.IDP_REDIRECT_URI,
    IDP_SILENT_REDIRECT_URI: process.env.IDP_SILENT_REDIRECT_URI,
    AWS_SECRET_BP: process.env.AWS_SECRET_BP,
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  assetPrefix: '/donde-comprar-chip-app/',
  async rewrites() {
    return [
      {
        source: '/donde-comprar-chip-app/_next/:path*',
        destination: '/_next/:path*',
      },
      {
        source: '/donde-comprar-chip/api/revalidate',
        destination: '/api/revalidate',
      },
      {
        source: '/donde-comprar-chip/api/provinces',
        destination: '/api/provinces',
      },
      {
        source: '/donde-comprar-chip/api/localities',
        destination: '/api/localities',
      },
      {
        source: '/donde-comprar-chip/api/salespoints',
        destination: '/api/salespoints',
      },
      {
        source: '/donde-comprar-chip/api/bigQueryTeco',
        destination: '/api/bigQueryTeco',
      },
      {
        source: '/donde-comprar-chip/api/bigQueryTest',
        destination: '/api/bigQueryTest',
      },
      {
        source: `/donde-comprar-chip-app/fonts/:path*`,
        destination: '/fonts/:path*',
      },
    ]
  },
  generateBuildId: async () => {
    return 'donde-comprar-chip'
  },
  images: {
    domains: [
      'publish-p32363-e109126.adobeaemcloud.com',
      'publish-p32363-e499238.adobeaemcloud.com',
    ],
  },
  webpack(config, options) {
    const { isServer } = options
    const MICROFRONTEND_BASE_URL = process.env.MICROFRONTEND_BASE_URL
    const location = isServer ? 'ssr' : 'chunks'
    Object.assign(config.experiments, { topLevelAwait: true })
    config.plugins.push(
      new NextFederationPlugin({
        name: 'host',
        remotes: {
          remote: `remote@${MICROFRONTEND_BASE_URL}/_next/static/${location}/remoteEntry.js`,
        },
        filename: 'static/chunks/remoteEntry.js',
      })
    )
    return config
  },
  async headers() {
    return [
      {
        source: '/donde-comprar-chip-app/fonts/:path*',
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=31536000, immutable',
          },
        ],
      },
      {
        source: '/fonts/:path*',
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=31536000, immutable',
          },
        ],
      },
    ]
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/donde-comprar-chip',
        permanent: true,
      },
    ]
  },
}
module.exports = nextConfig
