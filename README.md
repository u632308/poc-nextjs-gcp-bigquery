# PoC Landing Next conectada a Base de Datos BigQuery en GCP

Sitio en donde se puede visualizar la información consumida de una Base de Datos BigQuery en GCP.

## Tecnologías

- Frontend Next 14 con Api endpoints para llegar a la Base de Datos en Google Cloud Platform
- Node 18.17.0

## Api endpoints en /pages/api

- <http://localhost:3000/donde-comprar-chip/api/provinces>
- <http://localhost:3000/donde-comprar-chip/api/localities?province=tucuman>
- <http://localhost:3000/donde-comprar-chip/api/salespoints?province=tucuman&locality=graneros>
- <http://localhost:3000/donde-comprar-chip/api/bigQueryTeco>
- <http://localhost:3000/donde-comprar-chip/api/bigQueryTest>

## Getting started

```bash
nvm use 18.17.0

yarn install

yarn dev
```
